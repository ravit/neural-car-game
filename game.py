#!/usr/bin/python3

from car import car_c
import pygame as pg
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    K_s,
    KEYDOWN,
    QUIT,
)

import numpy as np
from PIL import Image
import pickle
import os, sys
import argparse
import json
import random
import matplotlib.pyplot as plt
import profile, pstats


class game_c:
    def __init__(self, learn_config=None, learned_weights=None, draw_graph=False, reverse=False):
        pg.init()
        self.population_score = []
        self.central_line = []
        self.all_sprites = pg.sprite.Group()

        self.img_map = None
        self.track_mask = None

        # Set up the drawing window
        self.screen = pg.display.set_mode([800, 600])
        self.clock = pg.time.Clock()

        # Load track, track mask and calculate central line
        self.prepare_map()
        self.central_line.reverse()
        car_c.track_center = self.central_line

        self.learn_config = None
        self.learned_weights = None
        self.maximal_inactivity = 10
        self.draw_graph = draw_graph

        start_car_postion = {"pos":(70, 260), "angle":90}
        if reverse:
            start_car_postion = {"pos":(70, 370), "angle":90+180}

        if learn_config != None:
            try:
                with open(learn_config) as f:
                    self.learn_config = json.load(f)
                print(self.learn_config)
            except:
                print("Parsing config failed")
                sys.exit()

            for i in range(self.learn_config["genetic"]["population_size"]):
                car = car_c(self.track_mask, self.screen, config=self.learn_config, **start_car_postion)
                self.all_sprites.add(car)

            if self.draw_graph:
                #plt.ion()
                self.figure = plt.figure()

        elif learned_weights != None:
            with open(learned_weights, 'rb') as f:
                self.learned_weights = pickle.load(f)

            # Generate player
            car = car_c(self.track_mask, self.screen, **start_car_postion)
            car.restore_setting(self.learned_weights)
            self.all_sprites.add(car)
        else:
            # Generate player
            car = car_c(self.track_mask, self.screen, **start_car_postion)
            self.all_sprites.add(car)

    def __del__(self):
        pg.quit()

    def find_central_line(self, track):
        """Find central line of track. It is used for score determining

        Args:
            track (image): Image of track

        Returns:
            list: Ordered vector of positions of central line form begin to end
        """

        from skimage.morphology import skeletonize, binary_closing
        track_center = []
        # Prepare array for track mask
        mask = np.zeros((track.get_width(), track.get_height()), dtype=np.uint8)

        print("mask size: {}x{}".format(*mask.shape))
        # In image find transparent parts (track) and mark them in array
        for x in range(mask.shape[0]):
            for y in range(mask.shape[1]):
                if track.get_at((x, y)).a == 0:
                    mask[x, y] = 1

        # Create skeleton (single line) from track
        skel = skeletonize(mask)
        mask = skel.astype(np.uint8)

        draw_img = np.zeros((track.get_width(), track.get_height(), 3), dtype=np.uint8)

        # Find begin of track
        begin = (0, 0)
        for x in range(1, mask.shape[0]):
            for y in range(1, mask.shape[1]):
                # Begin has only one neighbour colored, another 7 pixels are uncolored
                if mask[x, y] == 1 and mask[x-1, y] == 0 and mask[x+1, y] == 0 and mask[x, y+1] == 0 and mask[x+1, y+1] == 0 and mask[x-1, y-1] == 0 and mask[x-1, y+1] == 0 and mask[x+1, y-1] == 0:
                    begin = (x, y)
                if mask[x, y] == 1:
                    draw_img[x, y] = (255, 0, 0)
        draw_img[begin[0], begin[1]] = (0, 255, 0)

        # Trace all center line and create linear vector with position
        x = begin[0]
        y = begin[1]
        track_center.append(begin)
        while True:
            mask[x, y] = 255
            if mask[x-1, y] == 1:
                x -= 1
            elif mask[x+1, y] == 1:
                x += 1
            elif mask[x, y+1] == 1:
                y += 1
            elif mask[x, y-1] == 1:
                y -= 1
            elif mask[x+1, y+1] == 1:
                x += 1
                y += 1
            elif mask[x-1, y-1] == 1:
                x -= 1
                y -= 1
            elif mask[x-1, y+1] == 1:
                x -= 1
                y += 1
            elif mask[x+1, y-1] == 1:
                x += 1
                y -= 1
            else:
                break

            track_center.append((x, y))

        # Prepare extrapolation at begin
        begin_diffx = track_center[0][0] - track_center[1][0]
        begin_diffy = track_center[0][1] - track_center[1][1]

        extra = []
        posx = track_center[0][0]
        posy = track_center[0][1]

        # Extrapolate center line
        while True:
            posx += begin_diffx
            posy += begin_diffy
            if track.get_at((posx, posy)).a != 0:
                break
            extra.append((int(posx), int(posy)))
        extra.reverse()
        track_center = extra + track_center

        # Prepare extrapolation at end
        begin_diffx = track_center[-1][0] - track_center[-2][0]
        begin_diffy = track_center[-1][1] - track_center[-2][1]

        extra = []
        posx = track_center[-1][0]
        posy = track_center[-1][1]

        # Extrapolate center line
        while True:
            posx += begin_diffx
            posy += begin_diffy
            if track.get_at((posx, posy)).a != 0:
                break
            extra.append((int(posx), int(posy)))
        track_center = track_center + extra

        return track_center

    def prepare_map(self):
        self.img_map = pg.image.load("map.png")
        self.track_mask = pg.image.load("map-mask.png")

        # Find central line and save it for future usage (faster startup)
        if not os.path.isfile("central_line.pickle"):
            self.central_line = self.find_central_line(self.track_mask)
            with open("central_line.pickle", 'wb') as f:
                pickle.dump(self.central_line, f, pickle.HIGHEST_PROTOCOL)
        # If central line was saved load it
        else:
            with open("central_line.pickle", 'rb') as f:
                self.central_line = pickle.load(f)
        # Draw central line
        for pos in self.central_line:
            self.img_map.set_at(pos, (100, 100, 100))

    def tournament_selection(self, cars):
        """Select pair of cars for crossing. Use tournament selection method.
        Cars with higher score can have priority.

        Args:
            cars (list of car_c): List of cars prepared to crossing

        Returns:
            tuple: Pair of car to crossing
        """
        # Randomly select tournament_selection_size from cars
        indexes = random.sample(range(len(cars)), self.learn_config["genetic"]["tournament_selection_size"])
        group = [ cars[i] for i in indexes ]

        # Sort cars by score
        group.sort(reverse=True, key=lambda car: car["score"])
        # Select two cars with highest score to crossing
        return group[:2]

    def get_new_population(self):
        """Prepare new population.
        First select cars for crossing then cross them and at the end mutate part of the new population.

        Returns:
            list of car_c: New population
        """
        new_population = []
        all_cars = self.all_sprites.sprites()
        # Create indexer for selection
        cars = [ {"id": i, "score": car.get_score()} for i, car in enumerate(all_cars) ]
        cars.sort(reverse=True, key=lambda car: car["score"])

        # Select pairs and cross them
        for i in range(int(len(cars) - 1)):
            pair = self.tournament_selection(cars)
            new_population.append(all_cars[pair[0]["id"]].get_crossed(all_cars[pair[1]["id"]]))

        # Select cars for mutation and mutate them
        for i in random.sample(range(len(new_population)), int(min(len(new_population), self.learn_config["genetic"]["max_mutated_cars"]))):
            new_population[i].mutate()

        # Add elite car for future population (keep best genom in)
        self.elite = all_cars[cars[0]["id"]].get_crossed(all_cars[cars[0]["id"]])
        new_population.append(self.elite)

        # Add elite score to statistic
        self.population_score.append(all_cars[cars[0]["id"]].get_score())

        if self.draw_graph:
            self.figure.clear()
            plt.plot(np.array(self.population_score))
            plt.title("Score")
            plt.grid(True)
            plt.xlabel("population",fontsize=18)
            plt.ylabel("score",fontsize=18)
            plt.draw()
            plt.pause(0.01)

        return new_population

    def run_loop(self):
        # Run until the user asks to quit
        running = True
        while running:
            # Realtime
            self.clock.tick(60)

            # Process quit events
            for event in pg.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                            running = False
                    elif event.key == K_s:
                        if self.learn_config:
                            # Save elites actual weights
                            if os.path.exists(self.learn_config["output_filename"]):
                                os.remove(self.learn_config["output_filename"])
                            try:
                                with open(self.learn_config["output_filename"], 'wb') as f:
                                    pickle.dump(self.elite.get_setting(), f, pickle.HIGHEST_PROTOCOL)
                                    print("Learned weights saved")
                            except:
                                print("Saving failed")

                if event.type == pg.QUIT:
                    running = False

            # Get pressed keys
            pressed = pg.key.get_pressed()

            # Fill the background with white
            self.screen.fill((255, 255, 255))
            # Redraw map
            self.screen.blit(self.img_map, (0, 0))

            active_count = 0
            # Update all sprites
            for sprite in self.all_sprites.sprites():
                sprite.update(pressed)

                if self.learn_config and sprite.is_active():
                    active_count += 1

            if active_count <= 1 and self.learn_config:
                new_population = self.get_new_population()
                self.all_sprites.empty()
                for child in new_population:
                    self.all_sprites.add(child)

            # Redraw all sprites
            self.all_sprites.draw(self.screen)

            # Flip the display
            pg.display.flip()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Neural game', epilog='''
For saving best car for replay press 'S' in game window.
For exiting game press 'ESC' in game window.
For learning use typical command './game.py -l=learn_config.json -g'.
For replay use typical command './game.py -p=learned.pickle'.''')
    parser.add_argument('-l', '--learn', help="start learning with parameters defined in FILE (json)", metavar="FILE", dest="learn_config")
    parser.add_argument('-p', '--play', help="play game with neural network controlled car (network setting in FILE)", metavar="FILE", dest="play_config")
    parser.add_argument('-g', '--graph', dest="graph", action='store_true')
    parser.add_argument('-r', '--reverse', dest="reverse", action='store_true')

    args = parser.parse_args()
    print(args)

    game = game_c(args.learn_config, args.play_config, args.graph, args.reverse)
    game.run_loop()
    #profile.run('game.run_loop()', "statistic.stats")