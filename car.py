import math
import numpy as np
import pygame as pg
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)
import random
from ann import ann_c, ann_layer_c

class car_c(pg.sprite.Sprite):
    acceleration = 0.05
    decceleration = 0.1
    max_speed = 3
    max_reverse = 1
    steering = 2
    maximal_visibility = 200

    sensor_angles = [i for i in range(-90, 91, 45)]

    track_center = []

    def __init__(self, track_mask, screen, pos=(70, 260), angle=90, config=None):
        super(car_c, self).__init__()

        self.initial_angle = angle
        self.initial_pos = pos
        self.config = config

        self.speed = 0
        self.angle = angle
        self.posx, self.posy = pos

        self.track = track_mask
        self.screen = screen
        self.image = pg.image.load("car.png")
        self.image_first = self.image.copy()

        self.rect = self.image.get_rect()
        self.rect.center = pos
        self.rect.x = pos[0]
        self.rect.y = pos[1]

        self.rotate(self.angle)

        self.activity_timeout = 100
        self.maximal_inactivity = 100
        self.score_nonincreasing_maximal = 100
        self.score_nonincreasing_timeout = 100

        self.actual_score = 0
        self.max_score = 0

        self.ann = None
        if config:
            self.sensor_angles = config["car"]["angles"]
            self.maximal_visibility = config["car"]["visibility"]
            self.activity_timeout = config["car"]["maximal_inactivity"]
            self.maximal_inactivity = self.activity_timeout
            self.score_nonincreasing_timeout = config["car"]["score_nonincreasing_timeout"]

            self.ann = ann_c(len(self.sensor_angles))
            for layer in config["neural"]["layers"]:
                threshold = 0
                try:
                    threshold = layer["threshold"]
                except:
                    pass

                self.ann.add_layer(ann_layer_c(layer["num_neurons"], layer["activation_function"], threshold))
            self.ann.build()

    def get_setting(self):
        setting = {
            "ann": self.ann,
            "maximal_visibility": self.maximal_visibility,
            "sensor_angles": self.sensor_angles
        }
        return setting

    def restore_setting(self, setting):
        self.ann = setting["ann"]
        self.maximal_visibility = setting["maximal_visibility"]
        self.sensor_angles = setting["sensor_angles"]

    def rotate(self, angle):
        center = self.rect.center
        self.image = pg.transform.rotate(self.image_first, angle)
        self.rect = self.image.get_rect(center=center)

    def move(self, speed, angle):
        posx = self.posx + math.sin(((angle + 90) / 360) * 2*math.pi) * (speed)
        posy = self.posy + math.cos(((angle + 90) / 360) * 2*math.pi) * (speed)

        return (posx, posy, angle, speed)

    def apply_move(self, posx, posy, angle, speed):
        self.posx = posx
        self.posy = posy
        self.angle = angle
        self.speed = speed
        self.rect.x = int(self.posx)
        self.rect.y = int(self.posy)
        self.rect.center = (int(self.posx), int(self.posy))

        self.rotate(angle)

    def draw_detection_lines(self):
        for angle in self.sensor_angles:
            end = list(self.rect.center)
            end[0] += math.sin(((self.angle+90 + angle) / 360) * 2*math.pi) * 1000
            end[1] += math.cos(((self.angle+90 + angle) / 360) * 2*math.pi) * 1000
            pg.draw.line(self.screen, (255, 255, 100), self.rect.center, end)

    def get_sensors(self):
        sensors = []
        for angle in self.sensor_angles:
            difx = math.sin(((self.angle+90 + angle) / 360) * 2*math.pi)
            dify = math.cos(((self.angle+90 + angle) / 360) * 2*math.pi)

            x = self.posx
            y = self.posy
            length = 0
            while True:
                if self.track.get_at((int(x), int(y))).a != 0 or length > 700:
                    sensors.append(length)
                    break
                length += 1
                x += difx
                y += dify
        return sensors

    def ann_get(self):
        keys = (K_UP, K_DOWN, K_LEFT, K_RIGHT)
        outputs = self.ann.activate(self.get_normalized_sensors())
        out_keys = {}
        for i, out in enumerate(outputs):
            out_keys[keys[i]] = True if out > 0.5 else False

        return out_keys

    def process_keys(self, pressed_keys):
        angle = self.angle
        speed = self.speed

        if pressed_keys[K_UP]:
            speed += self.acceleration
            if speed > self.max_speed:
                speed = self.max_speed
        elif not pressed_keys[K_DOWN]:
            speed -= self.decceleration
            if speed < 0:
                speed = 0

        if pressed_keys[K_DOWN]:
            speed -= self.decceleration
            if speed < -self.max_reverse:
                speed = -self.max_reverse

        if pressed_keys[K_LEFT]:
            angle += self.steering

        if pressed_keys[K_RIGHT]:
            angle -= self.steering

        return (speed, angle)

    def is_active(self):
        if self.activity_timeout and self.score_nonincreasing_timeout:
            return True
        return False

    def get_normalized_sensors(self):
        sens = self.get_sensors()
        sens = np.array(sens).clip(0, self.maximal_visibility)
        sens = sens / self.maximal_visibility
        return sens

    def update(self, pressed_keys):
        last_score = self.get_score()
        if self.ann:
            pressed_keys = self.ann_get()

        if not self.ann or self.activity_timeout != 0:
            changed_params = self.process_keys(pressed_keys)
            coords = self.move(*changed_params)
            if(not self.collision(*coords)):
                self.apply_move(*coords)
            else:
                self.speed = 0

        self.calc_score()
        if self.ann:
            if self.get_score() > self.max_score:
                self.max_score = self.get_score()
                self.score_nonincreasing_timeout = self.score_nonincreasing_maximal
            elif self.is_active():
                self.score_nonincreasing_timeout -= 1
                if self.score_nonincreasing_timeout == 0:
                    self.image = pg.image.load("car_inactive.png")
                    self.image_first = self.image.copy()
                    self.rotate(self.angle)

            if last_score != self.get_score():
                self.activity_timeout = self.maximal_inactivity
            elif self.is_active():
                self.activity_timeout -= 1
                if self.activity_timeout == 0:
                    self.image = pg.image.load("car_inactive.png")
                    self.image_first = self.image.copy()
                    self.rotate(self.angle)

        self.draw_detection_lines()
        return self.get_normalized_sensors()

    def collision(self, posx, posy, angle, speed):
        for point in self.get_corners_iter(posx, posy, angle):
            pg.draw.circle(self.screen, (0, 0, 255), point, 2)
            if self.track.get_at(point).a != 0:
                return True
        return False

    def get_corners_iter(self, posx, posy, angle):
        for dirs in (-90, 90):
            for mult in (-1, 1):
                point = [int(posx), int(posy)]
                point[0] += int(math.sin(((angle + 90) / 360) * 2*math.pi) * (self.image_first.get_width()/2)) * mult
                point[1] += int(math.cos(((angle + 90) / 360) * 2*math.pi) * (self.image_first.get_width()/2)) * mult
                point[0] += int(math.sin(((angle + 90 + dirs) / 360) * 2*math.pi) * (self.image_first.get_height()/2)) * mult
                point[1] += int(math.cos(((angle + 90 + dirs) / 360) * 2*math.pi) * (self.image_first.get_height()/2)) * mult
                yield point

    def get_distance(self, a, b):
        # Euclidean distance
        #dist = math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)
        # Taxicab distance (faster cca 1/2 of time spend in euclidean distance)
        dx = a[0] - b[0]
        dy = a[1] - b[1]
        dist = 0
        if dx >= 0:
            dist += dx
        else:
            dist -= dx
        if dy >= 0:
            dist += dy
        else:
            dist -= dy
        return dist

    def calc_score(self):
        dist_min = 10000
        score = 0
        start_pos = self.actual_score
        if start_pos < 0:
            start_pos = 0
        for index, pos in enumerate(self.track_center):
            dist = self.get_distance((self.posx, self.posy), pos)
            if dist < dist_min:
                dist_min = dist
                score = index

        self.actual_score = score
        return score

    def get_score(self):
        return self.actual_score

    def get_crossed(self, partner):
        child = car_c(self.track, self.screen, self.initial_pos, self.initial_angle, self.config)

        for i in range(len(self.ann.layers)):
            child.ann.layers[i].weights = np.mean([self.ann.layers[i].weights, partner.ann.layers[i].weights], axis=0)

        return child

    def mutate(self):
        for i in range(len(self.ann.layers)):
            num_mutated = int(abs(np.random.normal(0, self.config["genetic"]["normal_ditribution_scale_weights_mutation"], 1)) * self.config["genetic"]["max_mutated_weights"])
            x = np.random.randint(0, self.ann.layers[i].weights.shape[0], num_mutated)
            y = np.random.randint(0, self.ann.layers[i].weights.shape[1], num_mutated)

            for pos in range(num_mutated):
                self.ann.layers[i].weights[x[pos], y[pos]] = self.ann.layers[i].weights[x[pos], y[pos]] * ((random.random()-0.5) * 4)
