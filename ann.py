import numpy as np


class ann_layer_c:
    def __init__(self, num_neurons, activation_fn='sigmoid', threshold = 1.0):
        self.weights = None
        self.num_neurons = num_neurons
        self.threshold = threshold
        self.activation_fn = self.fn_sigmoid

        if activation_fn == 'bin':
            self.activation_fn = self.fn_bin
        elif activation_fn == 'tanh':
            self.activation_fn = self.fn_tanh

    def build(self, previous_layer_neurons):
        """Generate random weights for layer.
        Each layer has previous_layer_neurons columns and num_neurons rows. This
        shape is usefull for matrix multiplication.

        Args:
            previous_layer_neurons (int): Number of inputs to thi layer
        """
        self.weights = (np.random.rand(self.num_neurons, previous_layer_neurons) * 2) - 1


    # Activation functions
    def fn_sigmoid(self, potentials):
        out = 1/(1 + np.exp(-potentials))
        return out

    def fn_tanh(self, potentials):
        out = np.tanh(potentials)
        return out

    def fn_bin(self, potentials):
        out = np.array(potentials)
        for i in range(len(out)):
            if out[i] > self.threshold:
                out[i] = 1.0
            else:
                out[i] = 0.0
        return out


    # Get outputs (activate ANN)
    def activate(self, inputs):
        """Activate ANN single layer.
        Get outputs from neurons based on their inputs.
        For each neuron output value is determined:
            out = activation function( sum( inputs * weights ) )

        Args:
            inputs (1D numpy.array): Input vector (neuron inputs)

        Returns:
            1D numpy.array: Output vector (output from neurons)
        """
        inputs = inputs.reshape((1, len(inputs)))

        out = self.weights * inputs
        out = out.sum(axis=1)

        out = self.activation_fn(out)
        return out

class ann_c:
    def __init__(self, num_inputs):
        self.layers = []
        self.num_inputs = num_inputs

    def add_layer(self, layer):
        """Add single layer to ANN.

        Args:
            layer (ann_layer_c): ANN layer
        """
        self.layers.append(layer)

    def activate(self, inputs):
        """Activate all ANN (get outputs).

        Args:
            inputs (1D numpy.array): Input vector (neuron inputs)

        Returns:
            1D numpy.array: Output vector (output from neurons)
        """
        state = inputs
        for layer in self.layers:
            state = layer.activate(state)

        return state

    def build(self):
        """Prepare ANN to work. Generate weights in layers.
        """
        num = self.num_inputs
        for l in self.layers:
            l.build(num)
            num = l.num_neurons


if __name__ == "__main__":
    ann = ann_c(3)
    ann.add_layer(ann_layer_c(10))
    ann.build()


    for i in range(10):
        print(ann.activate(np.array([0.01, 2, 3])))

